# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Setting import *
import xlwings as xw
from API import GetURLToken
from Lib import img_recognition


img_list = ["slot_menu.png", "slot_help.png", "slot_menu2.png", "slot_help2.png"]


class HelpPage:
    """ RSG電子-help頁面比對 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    @staticmethod
    def find_value_index(lang, game):
        """
        針對語系撈Excel文件內help資料
        Args:
            lang (str): 語系
            game(str): 遊戲名稱

        Returns:
            text_list (list): 該語系欄位所有資料
            locate_type(int): 定位元素寫法依文字是否有單(雙)引號。0->單引號，1->雙引號
        """

        text_list = []
        locate_type = 0

        # 打開Excel文件
        app = xw.App(visible=False, add_book=False)
        try:
            wb = app.books.open(f'Document/{slot_game_id[game]}.xlsx')
        except:
            print(f"Fail：無{slot_game_id[game]}文件\n")
            wb.close()
            app.quit()
            return False

        # 選擇要查找的工作表
        sht = wb.sheets['help']

        # 找出語系所在的欄位
        for i in sht.range(1, 1).expand('right'):
            if i.value == lang:
                text_list = sht.range(i.row, i.column).expand('down').value

        # 整理list資料
        try:
            for _ in range(3):
                text_list.pop(0)

            for i in text_list:
                if "\n" in i:
                    x = text_list.index(i)
                    text_list[x] = i.replace("\n", "")
                if "'" in i:
                    x = text_list.index(i)
                    text_list[x] = i.replace("'", "\'")
                if '"' in i:
                    locate_type = 1
                    x = text_list.index(i)
                    text_list[x] = i.replace('"', '\"')
        except:
            print(f"Fail：處理Excel文件時發生錯誤，遊戲為 {slot_game_id[game]}，文件內容為 {text_list}\n")
            wb.close()
            app.quit()
            return False

        wb.close()
        app.quit()

        return text_list, locate_type

    def check_help_page(self):
        """
        help頁面，內容比對
        """

        result_list = []
        ac = ActionChains(self.driver)
        print("測試項目：RSG電子Help頁面內容比對(*僅失敗時會顯示訊息*)\n")

        # 迴圈跑所有語系、遊戲
        for lang in lang_list:
            print(f"========測試語系：{lang_list[lang]}========")

            for id in slot_game_id:
                # 撈取Excel文件資料
                help_list, locate_type = self.find_value_index(lang=lang, game=id)
                if not help_list:
                    continue

                # 呼叫API取得遊戲網址
                url = GetURLToken.get_url_token(game_id=id, lang=lang)
                if url:
                    self.driver.get(url)
                else:
                    continue

                # 直版遊戲 - 聚寶財神、黃金摔角手
                if slot_game_id[id] in ['Caishen Fortunes', 'Luchadors']:
                    WebDriverWait(self.driver, 70).until(EC.element_to_be_clickable(
                        (By.XPATH, "//img[@class='getStartTxt']"))).click()

                    # 點擊漢堡列
                    img_coord = img_recognition.CoordLocator(self.driver, img_list[2])
                    img_path = img_coord.check_img()
                    click_result = img_coord.click_coordinate(img_path=img_path, re_load=True)
                    if not click_result:
                        self.get_img(f"Fail：{slot_game_id[id]}點擊漢堡列失敗")
                        result_list.append("False")
                        continue
                    else:
                        # 開啟Help頁面(點擊圖示座標)
                        time.sleep(2)
                        ac.reset_actions()
                        ac.pause(1).move_by_offset(812, 848).click().perform()
                        time.sleep(2)

                        # 切換iframe
                        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(
                            (By.XPATH, "//iframe[@id='contentIframe']")))

                        # 比對頁面內容文字
                        for i in help_list:
                            try:
                                if locate_type == 1:
                                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                        (By.XPATH, f"//*[contains(text(), '{i}')]")))
                                else:
                                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                        (By.XPATH, f'//*[contains(text(), "{i}")]')))
                            except:
                                print(f"Fail：頁面內容與文件有誤，語系為 {lang}，遊戲為 {slot_game_id[id]}")
                                print(f"無「{i}」此段文字\n")
                                result_list.append("False")

                # 直版遊戲 - 有請財神、麻將發了、魔龍傳奇
                elif slot_game_id[id] in ['CaishenComing', 'Rich Mahjong', 'Dragon Legend']:
                    # 點擊漢堡列
                    img_coord = img_recognition.CoordLocator(self.driver, img_list[2])
                    img_path = img_coord.check_img()
                    click_result = img_coord.click_coordinate(img_path=img_path, re_load=True)
                    if not click_result:
                        self.get_img(f"Fail：{slot_game_id[id]}點擊漢堡列失敗")
                        result_list.append("False")
                        continue
                    else:
                        # 開啟Help頁面(點擊圖示座標)
                        time.sleep(2)
                        ac.reset_actions()
                        ac.pause(1).move_by_offset(812, 848).click().perform()
                        time.sleep(2)

                        # 切換iframe
                        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(
                            (By.XPATH, "//iframe[@id='help_iframe']")))

                        # 比對頁面內容文字
                        for i in help_list:
                            try:
                                if locate_type == 1:
                                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                        (By.XPATH, f"//*[contains(text(), '{i}')]")))
                                else:
                                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                        (By.XPATH, f'//*[contains(text(), "{i}")]')))
                            except:
                                print(f"Fail：頁面內容與文件有誤，語系為 {lang}，遊戲為 {slot_game_id[id]}")
                                print(f"無「{i}」此段文字\n")
                                result_list.append("False")

                # 橫版遊戲
                else:
                    # 點擊漢堡列
                    img_coord = img_recognition.CoordLocator(self.driver, img_list[0])
                    img_path = img_coord.check_img()
                    click_result = img_coord.click_coordinate(img_path=img_path, re_load=True)
                    if not click_result:
                        self.get_img(f"Fail：{slot_game_id[id]}點擊漢堡列失敗")
                        result_list.append("False")
                        continue
                    else:
                        # 開啟Help頁面(點擊圖示座標)
                        time.sleep(2)
                        ac.reset_actions()
                        ac.pause(1).move_by_offset(200, 796).click().perform()
                        time.sleep(2)

                        # 切換iframe
                        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(
                            (By.XPATH, "//iframe[@id='help_iframe']")))

                        # 比對頁面內容文字
                        for i in help_list:
                            try:
                                if locate_type == 1:
                                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                        (By.XPATH, f"//*[contains(text(), '{i}')]")))
                                else:
                                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                        (By.XPATH, f'//*[contains(text(), "{i}")]')))
                            except:
                                print(f"Fail：頁面內容與文件有誤，語系為 {lang}，遊戲為 {slot_game_id[id]}")
                                print(f"無「{i}」此段文字\n")
                                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
