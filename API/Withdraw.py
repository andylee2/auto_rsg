import requests
import json
import time
from SGT import Crypt

clientID = "3df82c9b0af3"
clientSecret = "p@ssw0rd"
desKey = "12345678"
desIV = "76543210"
system_code = "QA"
apiUrl = "http://192.168.30.241:9001/WithBalance/Player/Withdraw"
payload = {'SystemCode': system_code,'WebId':'mobile','UserId':'test001','Currency':'NT','TransactionID':str(int(time.time_ns())),"Balance":1.00}

''' Example Description
Version docker image python:3.12.3-slim-bookworm 
pip install package
Package            Version
------------------ --------
pycryptodome       3.20.0
requests           2.31.0
'''


json_payload = json.dumps(payload)
post_data = Crypt.encrypt_des(desKey, desIV, json_payload)
unix_time = Crypt.get_unix_time()
sign = Crypt.get_md5_hash(clientID + clientSecret + str(unix_time) + post_data)

headers = {
     'Content-Type': 'application/x-www-form-urlencoded',
     'X-API-ClientID': clientID,
     'X-API-Signature': sign,
     'X-API-Timestamp': str(unix_time)
}
post_data = 'Msg=' + post_data
response = requests.post(apiUrl, headers=headers, data=post_data)
response_data = response.text

result = Crypt.decrypt_des(desKey, desIV, response_data)
print(result)
