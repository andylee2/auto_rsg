import requests
import json
from API.SGT import Crypt
from Setting import *


def get_url_token(game_id, lang, currency='NT'):
    clientID = "3df82c9b0af3"
    clientSecret = "p@ssw0rd"
    desKey = "12345678"
    desIV = "76543210"
    system_code = "QA"
    apiUrl = "http://192.168.30.241:9001/WithBalance/Player/GetURLToken"
    payload = {
        'SystemCode': system_code,
        'WebId': 'mobile',
        'UserId': 'auto_qa1',
        'Currency': currency,
        'UserName': 'Test-001',
        'GameId': game_id,
        'Language': lang,
        'ExitAction': ''
    }

    json_payload = json.dumps(payload)
    post_data = Crypt.encrypt_des(desKey, desIV, json_payload)
    unix_time = Crypt.get_unix_time()
    sign = Crypt.get_md5_hash(clientID + clientSecret + str(unix_time) + post_data)

    headers = {
         'Content-Type': 'application/x-www-form-urlencoded',
         'X-API-ClientID': clientID,
         'X-API-Signature': sign,
         'X-API-Timestamp': str(unix_time)
    }
    post_data = 'Msg=' + post_data
    try:
        response = requests.post(apiUrl, headers=headers, data=post_data)
    except Exception as e:
        print(e)
        return False
    response_data = response.text

    result = Crypt.decrypt_des(desKey, desIV, response_data)
    tmp = json.loads(result)
    if not tmp['ErrorCode'] == 0:
        print(f"Fail：取得{slot_game_id[game_id]}遊戲網址API發生錯誤")
        print(tmp, "\n")
        return False

    game_url = tmp['Data']['URL']+"&QAhost=192.168.30.241&QAport=10101"

    return game_url


# 取得網址測試用
url = get_url_token(game_id=59, lang='ms-MY')
print(url)
