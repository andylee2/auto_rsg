
import time
from Crypto.Cipher import DES
from Crypto.Util.Padding import pad, unpad
import base64
import hashlib

def get_unix_time():
    return int(time.time())


def encrypt_des(des_key, des_iv, payload):
    # Create DES instance
    cipher = DES.new(des_key.encode(encoding='utf-8'), DES.MODE_CBC, des_iv.encode(encoding='utf-8'))

    # The data to be encrypted (needs to be a multiple of 8 bytes)
    data = payload.encode(encoding='utf-8')

    # Encrypt data using MODE_CBC mode
    ct_bytes = cipher.encrypt(pad(data, DES.block_size))

    # Convert byte data to string using Base64 encoding
    ct_base64 = base64.b64encode(ct_bytes).decode('utf-8')
    return ct_base64


def decrypt_des(des_key, des_iv, cipher_text):
    # Create DES instance
    cipher = DES.new(des_key.encode(encoding='utf-8'), DES.MODE_CBC, des_iv.encode(encoding='utf-8'))

    # Decode byte data using Base64 encoding
    encrypted_data = base64.b64decode(cipher_text)

    # Decrypt data
    decrypted_data = unpad(cipher.decrypt(encrypted_data), DES.block_size).decode(encoding='utf-8')

    return decrypted_data


def get_md5_hash(param):
    md5_hash = hashlib.md5()
    md5_hash.update(param.encode('utf-8'))
    md5_hex = md5_hash.hexdigest()
    md5_str = str(md5_hex)
    return md5_str
