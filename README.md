# Auto_RSG(RSG自動化測試)

## 介紹

- RSG電子各功能自動化測試

## 功能

- help_page：依照語系針對help頁面內容文字比對

## 先決條件

- Python 3.9.13 版本。

## 執行

1. 安裝所需的依賴：
    
    ```bash
    pip3 install -r requirements.txt
    ```
    

2. 執行測試
   使用以下命令運行自動下注測試：
    ```bash
    python3 main.py
    ```
