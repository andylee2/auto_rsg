import sys
import os
import time
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
import help_page

sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))
options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()


class AutoRSG(unittest.TestCase):

    auto_help = help_page.HelpPage(driver, get_img)

    def test_help(self):
        """ Help頁面內容比對 """

        result = self.auto_help.check_help_page()
        self.assertEqual(result, True)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        AutoRSG("test_help"),
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title=f'RSG皇電自動化 - 測試環境: QA環境站',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()