# _*_ coding: UTF-8 _*_

# ================QA環境站================
# Port
port_list = {
    "10101": "10101(QA-pgSlot-CH3)",
    "10102": "10102(Prod-pgSlot)",
    "10103": "10103(QA-pgSlot-CH2)",
    "10200": "10200(QA-pgCascading)",
    "10201": "10201(Prod-pgCascading)",
    "10300": "10300(QA-pgFish)",
    "10301": "10301(Prod-pgFish)",
    "10400": "10400(QA-Bingo)",
    "10401": "10401(QA-EBingo)",
    "10410": "10410(QA-Bingo-1)",
    "10411": "10411(QA-Bingo-Sim-1)"
}

# 語系
lang_list = {
    "en-US": "英",  # 英文
    "zh-TW": "繁",  # 繁體中文
    "zh-CN": "簡",  # 簡體中文
    "th-TH": "泰",  # 泰文
    "ko-KR": "韓",  # 韓文
    "ja-JP": "日",  # 日文
    "en-MY": "緬",  # 緬文
    "id-ID": "印",  # 印尼文
    "vi-VN": "越",  # 越南文
    "ms-MY": "馬",  # 馬來西亞文
    "es-ES": "西",  # 西班牙文
    "lo-LO": "寮",  # 寮國文
}

# 遊戲代碼及遊戲名稱(依遊戲代碼排序)
slot_game_id = {
    1: "Fortune Thai", 2: "Magic Gem", 3: "Royal 777", 4: "Love City", 5: "Gold Chicken", 6: "Pharaoh",
    7: "Alibaba", 8: "Lucky Fruits", 10: "Jungle", 11: "Captain Hook", 12: "HUCA", 14: "Sweet Candy",
    15: "Fire Spin", 16: "Popeye", 17: "Crazy Doctor", 18: "Nonstop", 19: "5 Dragons", 21: "72 Changes",
    23: "Mermaid", 24: "Buffalo", 25: "Wild Panda", 26: "Lucky Thailand", 27: "God of Wealth", 28: "Lucky Dragon",
    29: "HUSA", 30: "Dragon King", 31: "Tiki Party", 32: "Goblin Miner", 33: "Lucky Bar", 34: "Africa",
    35: "Wizard Store", 36: "MrDoggy", 37: "Disco Night", 38: "Horror Nights", 39: "China Empress",
    40: "FuWaFaFa", 41: "Tarzan", 42: "Jalapeno", 43: "Piggy Punch", 44: "Sevens High", 45: "Kunoichi",
    46: "Ninja", 47: "Jelly 27", 48: "Angry Bear", 49: "Poseidon", 50: "Dancing Lion", 51: "Medusa", 52: "Medea",
    53: "Neon Circle", 55: "Get High", 56: "Cowboy", 58: "The Little Match Girl", 59: "Mystery Panda",
    60: "Hip Hop Monkey", 61: "Book of Gold", 65: "Tai Chi", 66: "Golden Leaf Clover", 68: "Wizard Store Gold",
    70: "Rats Money", 72: "Songkran", 73: "Elf Archer", 75: "Luchadors", 76: "Bear Kingdom", 78: "Royal 7777",
    81: "Dragon King2", 82: "Pharaoh II", 90: "Dragon Fight", 100: "Roma", 111: "Happy Farm", 112: "Power of Thor",
    113: "Chin Shi Huang", 114: "Caishen Fortunes",
    121: "Legend of Lu Bu",
}

# 幣別代碼
currency_list = {
    "NT": "新台幣",
    "HK": "港元",
    "IDR": "印尼盾",
    "JPY": "日圓",
    "KRW": "韓圓",
    "MYR": "馬幣",
    "RMB": "人民幣",
    "SGD": "新加坡元",
    "THB": "泰銖",
    "USA": "美元",
    "MMK": "緬甸緬元",
    "VND": "越南盾",
    "INR": "印度盧比",
    "PHP": "披索",
    "EUR": "歐元",
    "GBP": "英鎊",
    "USDT": "泰達幣",
    "MYR2": "馬幣 100",
}

# 測試單一遊戲or數個遊戲時，自行填入代碼及遊戲名稱
# slot_game_id = {1: "Fortune Thai"}

# 有問題遊戲暫放
# 117: "Rich Mahjong", 119: "Dragon Legend", 120: "CaishenComing", 2001: "Energy Combo"
